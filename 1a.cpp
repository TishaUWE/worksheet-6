#include <iostream>
#include <list>
#include <algorithm>

using namespace std;

int main()
{
    list<int> myList;
    myList.push_front(1);
    myList.push_front(2);
    myList.push_front(3);
    myList.push_front(4);
    myList.push_front(5);

    cout << "Original list: ";
    for (auto i : myList)
        cout << i << " ";
    cout << endl;

    reverse(myList.begin(), myList.end());

    cout << "Reversed list: ";
    for (auto i : myList)
        cout << i << " ";
    cout << endl;
    return 0;
}
