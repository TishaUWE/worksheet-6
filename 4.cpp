#include <iostream>
#include <queue>
#include <map>
#include <string>
#include<cstring>
using namespace std;

// Patient class to store patient information
class Patient
{
public:
    string patient_id;//storing alphanumeric variable
    int social_id;
    char occupation[25];
    char disease_reported[25];
    char treatment_prescribed[25];
    bool previous_treated;
    int token_number;
    string dateofVisit;

    Patient(string patient_id, int social_id, char occupation[25], char disease_reported[25], char treatment_prescribed[25], bool previous_treated, int token_number, string dateofVisit)
    {
        this->patient_id = patient_id;
        this->social_id = social_id;
        strcpy(this->occupation, occupation);
        strcpy(this->disease_reported ,disease_reported);
        strcpy(this->treatment_prescribed , treatment_prescribed);
        this->previous_treated = previous_treated;
        this->token_number = token_number;
        this->dateofVisit = dateofVisit;
       
    }
};

// Queues for each department
queue<Patient> dermatology_queue;
queue<Patient> cardiology_queue;
queue<Patient> gynecology_queue;
queue<Patient> pathology_queue;
queue<Patient> other_queue;

// Map to keep track of patients per day
map<string, int> patients_per_day;

int token_number = 1;
 
// Function to assign a unique token number to each patient and add them to the appropriate queue
void allotTokenNumber(string patient_id, int social_id, char occupation[25],char disease_reported[25],char treatment_prescribed[25], bool previous_treated, string dateofVisit)
{
   
    Patient new_patient(patient_id, social_id, occupation, disease_reported, treatment_prescribed, previous_treated, token_number, dateofVisit);
    cout << patient_id << " " << social_id << " " <<occupation << " " <<disease_reported << " " <<treatment_prescribed << " " << token_number << " " <<dateofVisit << endl;
    token_number++;

    // incrementing the number of patients per day
    patients_per_day[dateofVisit]++;
    
   
    if (strcmp(disease_reported , "Dermatology")==0)
    {
        dermatology_queue.push(new_patient);
    }
    else if (strcmp (disease_reported ,"Cardiology") == 0)
    {
        cardiology_queue.push(new_patient);
    }
    else if (strcmp (disease_reported , "Gynecology") == 0)
    {
        gynecology_queue.push(new_patient);
    }
    else if(strcmp (disease_reported , "Pathology") == 0)
    {
        pathology_queue.push(new_patient);
    }
    else
    { 
        other_queue.push(new_patient);
    }

    
}

int total_patients_in_queue(char department[25])
{
    int total_patients = 0;
    if (strcmp(department ,"Dermatology") == 0){
        total_patients = dermatology_queue.size();
    } else if(strcmp (department,"Cardiology") == 0) {
        total_patients = cardiology_queue.size();
    } else if(strcmp (department ,"Gynecology")==0){
        total_patients = gynecology_queue.size();
    } else if(strcmp(department ,"Pathology") == 0) {
        total_patients = pathology_queue.size();
    } else {
        total_patients = other_queue.size();
    }
    return total_patients;
}



double average_patients_per_day() {
    int total_patients = 0;
    return ((total_patients + patients_per_day.size())/ 2);

}

    int main() {
        
    cout << "Patient information" <<endl;

    allotTokenNumber("1A", 111111111, "Security analyst", "Neurology", "Antibiotics", false, "2018-02-18");
    allotTokenNumber("2B", 222222222, "House wife", "Dentistry", "Oral gel", true, "2018-02-20");
    allotTokenNumber("3C", 333333333, "Doctor", "Cardiology", "Heart medication", true, "2018-02-15");
    allotTokenNumber("4D", 444444444, "Engineer", "Dermatology", "Retiderm", false, "2018-02-14");
    allotTokenNumber("5E", 555555555, "Student", "Gynecology", "Antibiotics", false, "2018-02-20");

    //Patients in queue
    int total_patients_dermatology = total_patients_in_queue("Dermatology");
    cout << "Total patients in Dermatology queue: " << total_patients_dermatology << endl;
    int total_patients_cardiology = total_patients_in_queue("Cardiology");
    cout << "Total patients in Cardiology queue: " << total_patients_cardiology << endl;
    int total_patients_gynecology = total_patients_in_queue("Gynecology");
    cout << "Total patients in Gynecology queue: " << total_patients_gynecology << endl;
    int total_patients_pathology = total_patients_in_queue("Pathology");
    cout << "Total patients in Pathology queue: " << total_patients_pathology << endl;
    int total_patients_other = total_patients_in_queue("Other");
    cout << "Total patients in Other queue: " << total_patients_other <<endl;
    
    //Waiting patients
    int total_patients_waiting = dermatology_queue.size() + cardiology_queue.size() + gynecology_queue.size() + pathology_queue.size() + other_queue.size();
    cout << "Total patients waiting in the hospital: " << total_patients_waiting << endl;
       
    cout << "Average number of patients arriving per day: " <<average_patients_per_day<< endl;

    return 0;

}

