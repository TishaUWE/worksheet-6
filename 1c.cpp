#include <iostream>
#include <queue>
#include <vector>
#include <algorithm>

using namespace std;

void sortQueue(queue<int> &q)
{
    // Transfer the elements of the queue to a vector
    vector<int> vec;
    while (!q.empty())
    {
        vec.push_back(q.front());
        q.pop();
    }

    // Sort the vector
    sort(vec.begin(), vec.end());

    // Transfer the elements back to the queue
    for (auto i : vec)
        q.push(i);
}

int main()
{
    queue<int> myQueue;
    myQueue.push(3);
    myQueue.push(1);
    myQueue.push(4);
    myQueue.push(2);

    sortQueue(myQueue);
    // Check the size of the queue
    cout << "The size of Queue is: " << myQueue.size() << endl;

    // Access the front and back elements of the queue
    if (!myQueue.empty())
    {
        cout << "Front element: " << myQueue.front() << endl;
        cout << "Back element: " << myQueue.back() << endl;
    }
    cout << "After removing elements" << endl;
    // Remove elements from the queue
    if (!myQueue.empty())
    {
        myQueue.pop();
        cout << "Queue size: " << myQueue.size() << endl;
    }
    if (!myQueue.empty())
    {
        cout << "Front element: " << myQueue.front() << endl;
    }

    cout << "Sorted queue: ";
    while (!myQueue.empty())
    {
        cout << myQueue.front() << " ";
        myQueue.pop();
    }
    cout << endl;

    return 0;
}
