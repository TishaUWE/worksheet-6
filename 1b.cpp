#include <iostream>
#include <stack>
#include <algorithm>

using namespace std;

void decimal_to_binary(int decimal)
{
    stack<int> binary;
    while (decimal > 0)
    {
        binary.push(decimal % 2);
        decimal = decimal / 2;
    }
    cout << "Binary: ";
    while (!binary.empty())
    {
        cout << binary.top();
        binary.pop();
    }
    cout << endl;
}

int main()
{
    int decimal = 15;
    decimal_to_binary(decimal);
    return 0;
}
